all: build run
build:
	mkdir -p build/ && gcc -o build/itimer11 itimer11.c
run:
	build/itimer11
clean:
	rm -rf build/
